## Integrated XYZ machines from Urumbu axis components.

The goal is to make an integrated xyz machine from the urumbu axis parts that can machine PCBs and cut wax/wood in a volume of 150x150x50 mm.

### Machine using xy table and a fixed cam-driven z-axis.

This machine is documented [here.](../UrumbotXY/README.md).  It does a nice job of machining PCB traces, but with a few mm of travel, is a specialist.

### Machine with short z-axis moving on x-beam.

A short z-axis was mounted on the x-beam of the Core-xy table.  The first iteration of this design lacked the stifness needed to machine PCB's and wax.  [Details are here.](./moving_z_axis/README.md) 

### Machine with short z-axis fixed to the frame.  Moving table.

The same axes were rearranged so that the z-axis was fixed relative to the frame, and the xy- carriage supports a moving table.  This arrangement was nearly stiff enough to machine PCBs and wax.  [Details](./fixed_axis/README.md)


### Machine with short z-axis fixed to the frame.  Moving table with double x-beam.  May 2022

[Details](./fixed_z_axis_may_2022/README.md)
