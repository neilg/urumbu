## Integrated XYZ machine

### Mar 11, 2022.  

Build frame for slightly smaller machine designed to mill in a 150x150x50 mm volume.


<img src=./img/xyz_frame.jpg width=40%>

###  Mar 14-20, 2022.

Assemble.  When integrating z-axis on moving beam, it is necessary to insert at least 5mm between the x-beam plate and the z-axis plate, to allow clearance for screws.  If we take this approach seriously, we need to revisit the spacing of these plates so that the stepper capstan sits closer to the motor body.  The line attachments are made in this iteration using 3D printed clamps on the plates.  Perhaps better to integrate these attachments into plate design.

<img src=./img/z_axis.jpg width=40%>

<img src=./img/assembled.jpg width=40%>

<img src=./img/integrated.jpg width=40%>


###  Mar 22-23, 2022.

#### Ran tests for force and milling with this setup.  Information about the setup:

 - Use green fishing line that has been stretched for several days.  Newly tightened.
 - Measured stiffness of xy stage dF/dy = 40 N/mm.
 - At tip of end mill dF/dy = 12 N/mm and dY/dz = 9 N/mm.  This feels weak.
 - When force is applied to endmill, the z axis rotates around the beam, and the string stretches as well.  Hard to sort this out, but there are several effective springs in series that would all need to be stiffened up.
 - In order for stepper motor to hold wieght of spindle, I need to set vref on stepper chip to 2.0V (1.5V does not work).  At this setting, the chip gets warm to touch, but seems to be happy for hours.
 - Genmitsu spindle running at 24V, ~1.5A.  Advertised to be 20kRPM at 24 V.  Not yet confirmed.
 - Runout measured on this spindle is 0.002" at the endmill, 0.0005" at the motor shaft.  Most of the slop is in the collet.



#### Results

 - While cutting wood block, the 1/8" single flute end mill is pulled in x,y, and z by the cutting forces.  The video and photo shows the rough cutting and the wobble as the cutting progresses.

<img src=./img/wood_mill.jpg width=40%></br>

<img src=./img/moving_z_wood.cmp.mp4 width=40%>

Results when attempting to cut a PCB show the same problems.  When the 1/64" endmill engages the material, it is pulled down by ~1 mm into the board.

###  Conclusions

This configuration is not stiff enough at the endmill tip to mill successfully.
