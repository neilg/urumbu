## Integrated XYZ machine with fixed short z-axis.  Mar 23-25, 2022.  

###  Assembly

Turn frame upside down, add supports, and suspend the z-axis above the moving xy table.

<img src=./img/upside_down.jpg width=40%>

<img src=./img/x_y_assembled.jpg width=40%>

<img src=./img/before_string.jpg width=40%>

Add strings and connections:

<img src=./img/fixed_z_string_connection.jpg width=40%>

Add the table:

<img src=./img/table_attachment.jpg width=40%>

Assembled:

<img src=./img/fixed_z_assembled.jpg width=40%>


#### z-axis details.

<img src=./img/z_axis_string.jpg width=40%>
<img src=./img/z_axis_string_side.jpg width=40%>


#### Some details

 - Measured dF/dz= 30 N/mm at spindle axis.  About 3X larger than the version with compound stage.
 - Set up software arguments.  While facing machine, left motor is "b" is "1" and for both motors, reverse=False.

### Results

Below is a video demonstrating 3D motion from g-code.  Moving 100mm in x and y, 25 mm in z.  Total motion is nearly 150x150x50 mm.  Speed in this video 40 mm/s.

<img src=./img/xyz.cmp.mp4 width=100%><br>

PCB miiling:  Set speed at 4 mm/s.  As seen in the video, the tool is still pulled into the board as it cuts, by about 0.004".  

<img src=./img/pcb.cmp.mp4 width=100%><br>

Here's the board result:  The channels are much deeper than specified, ~ 0.2 - 0.5 mm.  Some distortion in the trace shapes.

<img src=./img/fixed_z_pcb.jpg width=40%>

And wax milling:  At 10? mm/s.  The 1/8" single-flute endmill runs with some vibration and is pulled downward when plunging, but almost machines well.

<img src=./img/wax_milling.cmp.mp4 width=100%><br>

Wood milling result is better than moving-z configuration:

<img src=./img/fixed_z_mill_wood.jpg width=50%><br>

Loading work on the bed:  This frame gives plenty of room to move bed back for tool changes and to move bed forward for access to the work.

<img src=./img/bed_rear.jpg width=25%>
<img src=./img/bed_center.jpg width=25%>
<img src=./img/bed_front.jpg width=25%>

### Conclusions

This configuration almost works for both PCBs and wax, with dF/dz = 30 N/mm at spindle, and unknown but measurable stiffness in the rest of the mechanism.

#### Successes

 - Separating the axes makes it easier to evaluate and improve stiffness of either.
 - Nice geometry for access to work.
 - Good to have strings underneath frame.  
 - Motors are all fixed to frame, and connections don't have to flex.
 - Almost works.  The nice results from the limited-z machine (with 2 mm cam action) on PCBs indicates that we are getting close to sufficiently good resolution and stiffness for PCBs, and the wax results are encouraging.


#### To be done.

 - Stiffen up z-axis
 	- Better string, two strings, belt, leadscrew.
 	- Evaluate bearing design and improve if it makes sense to do so.
 - Measure plate compliance (better word than stiffness?) and increase roller separation to stabilize if it makes sense.

 ###  Evaluating and adding stiffness to z-axis.

 #### Adding more string

  - Confirmed about 30 N/mm stiffness on z axis at the spindle axis.
  - Measured force and motion with a force sensor attaced to the plate that move the z-spindle, so that little torquing of the bearing was involved.  The result is identical to the spindle measurement, about 30 N/mm, indicating that most of the axis compliance is associated with the belt and motor, rather than the rubber bearings.
  - Braided three strands together.  Upon reassembling, the stiffness increased by ~20 percent - not significant.  I can see that the motor is turning slightly as force is applied.  Looks like stepper motor torque is the limiting factor.  I will reduce the motor spindle diameter by a factor of three or so.  Picture below is of three strands braided together on z-axis.

<img src=./img/braided_braid.jpg width=40%>


### Progress as of 4/21/22

#### Replaced loop with direct drive:

Kevlar string wrapped around stepper axle, tension in string opposed by spindle wieght.  PCB traces come out well, using conical bit. This is preliminary - needs more quality time!


<img src=./img/Kevlar_drive.jpg width=40%>


<img src=./img/pcb_traces_4_21.jpg width=40%>

#### Next steps:

 - Quantify properties of Kevlar string vs Steel vs fishline.
 - Verify machining of wax.
 - Add second x-axis beam.
 - Characterize machine with Kevlar string, if that makes sense.
 
#### Progress as of 4/27/22

 - Kevlar string measured.  Creep and hysteresis were below measurement sesitivity on my apparatus.  Elastic constant K = 9.5kN.  That is, for one meter, dF/dx = 9.5 N/mm. <a href="https://gitlab.cba.mit.edu/classes/865.21/projects/machinemetrology">Details.</a>
 - With this string, wrapped directly around motor shaft, and using weight of the spindle as the sole downward force, it is possible to mill a reasonable PCB with the conical bit, and wax milling goes well.  Measured dF/dz is much higher than before, 120 N/mm.  
 - The xy stage by itself has dF/dy ~ 40 N/mm.  With wax block on table, at the top of the block dF/dy = 30 N/mm.  Extra compliance is due to rotation about the single x-beam as a function of sideways flex of bearings.  
 - Applied kevlar string to the xy stage.  Now the stiffness of the stage, dF/dy, has doubled to 80 N/mm.  When the table is added, and a wax block attached, the stiffness at the top of the block is still low, about 40 N/mm.  Now the 20mm width of the x-beam is the limiting design parameter.
 - Made an eccentric nut as a bearing adjuster, following design from Ender machine.  See below.

[Youtube video of wax milling](https://youtube.com/shorts/Uz45yHX_n6A?feature=share)
<img src=./img/milled_wax.jpg width=30%>
<img src=./img/adjuster.jpg width=40%>

#### Next steps.

 - Make a double-beam for the x-axis.  This will triple the torque arm for the stage.  
 - Looks like, with the stiffer string, that the xy compliance is dominated by the stepper motor holding torque.  Let's cut the drum radius in half, and increase the holding current to 0.6A
 - Once we have a configuration that we like, it would be good to do a more detailed analysis of the compliance budget.  Good to use the Instron to see it it has more to offer than my basement workbench system.  

