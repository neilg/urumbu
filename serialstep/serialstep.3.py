#
# serialstep.3.py
#    serial step-and-direction, 3 ports, multiprocessing
#
# Neil Gershenfeld  5/25/21
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#
import serial,sys,time,signal,multiprocessing
#
# parse command line
#
if (len(sys.argv) != 6):
   print("command line: serialstep.2.py port0 port1 port2 speed delay")
   sys.exit()
device0 = sys.argv[1]
device1 = sys.argv[2]
device2 = sys.argv[3]
baud = int(sys.argv[4])
delay = float(sys.argv[5])
#
# open ports
#
print('open '+device0+' at '+str(baud)+' delay '+str(delay))
port0 = serial.Serial(device0,baudrate=baud,timeout=0)
print('open '+device1+' at '+str(baud)+' delay '+str(delay))
port1 = serial.Serial(device1,baudrate=baud,timeout=0)
print('open '+device2+' at '+str(baud)+' delay '+str(delay))
port2 = serial.Serial(device2,baudrate=baud,timeout=0)
ports = [port0,port1,port2]
count = 0;
#
# global variables 
#
maxcount = 5000;
forward = b'f'
reverse = b'r'
#
# shared memory variable
#
count = multiprocessing.Value('i',0,lock=False)
#
# worker event handlers
#
def worker0(id,count):
   localcount = 0
   while (1):
      if (localcount != count.value):
         localcount = count.value
         if (localcount < maxcount/6):
            ports[id].write(forward)
         elif ((localcount >= 3*maxcount/6) and (localcount < 4*maxcount/6)):
            ports[id].write(reverse)
         elif ((localcount >= 4*maxcount/6) and (localcount%2 == 0)):
            ports[id].write(forward)
#
def worker1(id,count):
   localcount = 0
   while (1):
      if (localcount != count.value):
         localcount = count.value
         if ((localcount >= maxcount/6) and (localcount < 2*maxcount/6)):
            ports[id].write(reverse)
         elif ((localcount >= 3*maxcount/6) and (localcount < 4*maxcount/6)):
            ports[id].write(forward)
         elif ((localcount >= 4*maxcount/6) and (localcount%3 == 0)):
            ports[id].write(reverse)
#
def worker2(id,count):
   localcount = 0
   while (1):
      if (localcount != count.value):
         localcount = count.value
         if ((localcount >= 2*maxcount/6) and (localcount < 3*maxcount/6)):
            ports[id].write(forward)
         elif ((localcount >= 3*maxcount/6) and (localcount < 4*maxcount/6)):
            ports[id].write(reverse)
         elif ((localcount >= 4*maxcount/6) and (localcount%4 == 0)):
            ports[id].write(forward)
#
# alarm event handler
#
def alarm(signum,stack):
   count.value += 1
   if (count.value == maxcount):
      count.value = 0
#
# start workers
#
p0 = multiprocessing.Process(target=worker0,args=(0,count))
p0.start()
p1 = multiprocessing.Process(target=worker1,args=(1,count))
p1.start()
p2 = multiprocessing.Process(target=worker2,args=(2,count))
p2.start()
#
# start alarm
#
signal.signal(signal.SIGALRM,alarm)
signal.setitimer(signal.ITIMER_REAL,delay,delay)
#
# wait for alarms
#
while (1):
   0  
