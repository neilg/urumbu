#
# serialstep.4.py
#    serial step-and-direction, 4 ports, multiprocessing
#
# Neil Gershenfeld  5/25/21
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#
import serial,sys,time,signal
#
# parse command line
#
if (len(sys.argv) != 7):
   print("command line: serialstep.2.py port0 port1 port2 port3 speed delay")
   sys.exit()
device0 = sys.argv[1]
device1 = sys.argv[2]
device2 = sys.argv[3]
device3 = sys.argv[4]
baud = int(sys.argv[5])
delay = float(sys.argv[6])
#
# open ports
#
print('open '+device0+' at '+str(baud)+' delay '+str(delay))
port0 = serial.Serial(device0,baudrate=baud,timeout=0)
print('open '+device1+' at '+str(baud)+' delay '+str(delay))
port1 = serial.Serial(device1,baudrate=baud,timeout=0)
print('open '+device2+' at '+str(baud)+' delay '+str(delay))
port2 = serial.Serial(device2,baudrate=baud,timeout=0)
print('open '+device3+' at '+str(baud)+' delay '+str(delay))
port3 = serial.Serial(device3,baudrate=baud,timeout=0)
#
# global variables 
#
count = 0
maxcount = 5000;
forward = b'f'
reverse = b'r'
#
# alarm event handler
#
def alarm(signum,stack):
   global count
   count += 1
   if (count < maxcount/7):
      port0.write(forward)
   elif ((count >= 1*maxcount/7) and (count < 2*maxcount/7)):
      port1.write(reverse)
   elif ((count >= 2*maxcount/7) and (count < 3*maxcount/7)):
      port2.write(forward)
   elif ((count >= 3*maxcount/7) and (count < 4*maxcount/7)):
      port3.write(reverse)
   elif ((count >= 4*maxcount/7) and (count < 5*maxcount/7)):
      port0.write(reverse)
      port1.write(forward)
      port2.write(reverse)
      port3.write(forward)
   elif (count >= 5*maxcount/7):
      if (count%2 == 0):
         port0.write(forward)
      if (count%3 == 0):
         port1.write(reverse)
      if (count%4 == 0):
         port2.write(forward)
      if (count%5 == 0):
         port3.write(reverse)
   if (count > maxcount):
      count = 0
#
# start alarm
#
signal.signal(signal.SIGALRM,alarm)
signal.setitimer(signal.ITIMER_REAL,delay,delay)
#
# wait for alarms
#
while (1):
   0  
