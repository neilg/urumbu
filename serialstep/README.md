# USB 3 power, serial step-and-direction

## [timing tests](../serialtimetest/README.md)

## [parts](../components/README.md)

## DRV8428 motor controller

<img src="http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428.png" width="45%">

<img src="http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428.jpg" width="45%">

<br>

<img src="http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428-D11C.png" width="45%">

<img src="http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428-D11C.jpg" width="45%">

<br>

<img src="http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428-D11C-NEMA17.png" width="45%">

<img src="http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428-D11C-NEMA17.jpg" width="45%">

## one node, 1/8 microstepping

[host code](serialstep.1.py), [embedded code](serialstep.ino), <a href="serialstep.mp4">video</a>

<img src="serialstep.mp4" width="100%">

## one node, 1/2-1/256 microstepping

[embedded code](http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428-D11C.ino), <a href="http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428-D11C.mp4">video</a>

<img src="http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428-D11C.mp4" width="100%">

## two nodes, 1/8 microstepping

[host code](serialstep.2.py), [embedded code](serialstep.ino), <a href="serialstep.2.mp4">video</a>

<img src="serialstep.2.mp4" width="100%">

## three nodes, 1/8 microstepping, multiprocessing

[host code](serialstep.3.py), [embedded code](serialstep.ino), <a href="serialstep.3.mp4">video</a>

<img src="serialstep.3.mp4" width="100%">

i7-1070H laptop (6 cores, 12 threads), Ubuntu 20.04

htop shows 4 (virtual) cores in use (1 for alarm process, 3 for node processes):

<img src="serialstep.3.png" width="75%">

## four nodes, 1/8 microstepping, multiprocessing

[host code](serialstep.4.multi.py), [embedded code](serialstep.ino), <a href="serialstep.4.multi.mp4">video</a>

<img src="serialstep.4.multi.mp4" width="100%">

<img src="serialstep.4.multi.png" width="75%">

## four nodes, 1/8 microstepping, sequential

[host code](serialstep.4.seq.py), [embedded code](serialstep.ino), <a href="serialstep.4.seq.mp4">video</a>

<img src="serialstep.4.seq.mp4" width="100%">

<img src="serialstep.4.seq.png" width="75%">

