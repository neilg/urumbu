import numpy as np
import math
import matplotlib.pyplot as plt


def main():
    peak = 2400
    n_micro = 32

    # 4 phase cycle
    n_tot = n_micro * 4
    i = np.arange(n_tot)
    theta = i * 2 * math.pi/n_tot
    table_ideal = peak * np.sin(theta)
    table_ideal[2 * n_micro:] = 0
    table = np.round(table_ideal).astype(np.int32)

    plt.figure()
    plt.plot(table_ideal, "b")
    plt.step(table, "r")
    plt.title("xIN PWM lookup")
    plt.xlabel("index")
    plt.ylabel("value")
    plt.legend(["ideal", "quantized"])
    plt.show()

    print("int sine_table[] = {")
    for i in range(n_tot):
        print("{:4d}".format(table[i]), end="")
        if i == n_tot - 1:
            print("\n};", end="")
            break
        print(",", end="")
        if (i + 1) % n_micro == 0:
            print("\n", end="")


if __name__ == "__main__":
    main()
