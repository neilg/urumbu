# DRV8428P motor controller

This is a variant based on the DRV8428P which is a dual H-bridge chip in the DRV8428 family. Manufacturing files are under [./board/](./board/).

<img src="board/hello.DRV8428P-D11C-NEMA17.png" width="45%">

<img src="board/hello.DRV8428P-D11C-NEMA17.jpg" width="45%">

## Full step

<img src="full_step.mp4" width="100%">

## Microstepping

<img src="microstep.mp4" width="100%">

Microstepping up to 32 is achieved through PWM on the H-bridge inputs. A high frequency is needed, therefore 4 channels of TCC0 are used:

| Pin  | TCC0 channel | MUX |
|------|--------------|-----|
| PA8  |      2       |  E  |
| PA8  |      3       |  E  |
| PA14 |      0       |  F  |
| PA15 |      1       |  F  |

TCC0 is then setup in normal PWM mode with a period of 2400, yielding 20kHz with a prescaler of 1. This is more than 11 bits of resolution for a nice, smooth sine signal on each of the PWM inputs:

<img src="img/pwm_lookup.png" width="100%">

## Measuring angle with 1/32 microstepping.

Cut and populated this board Mar. 3, 2022.  Stepper used has measured 1.8 Ohm coil resistance, and is labelled "Shinano Kenshi STP-42D201-37".  This is not the standard stepper we have been using.  With this motor, the driver chip is not happy at high current.  Need to be lowered to 1.2V (0.4 A) for motor to run well.

<img src="img/pwm_board.jpg" width="50%">

Set up with pivot/mirror 3378mm distant from ruler.  Beam translation 212 mm corresponds to one 1.8 degree step. The total excursion in the 32 steps shown is one full step.  This result obtained with steppers from Neil's stock, impedance 1.6 Ohm per phase.  Similar result for a second identical board, and similar result driving another stepper with impedance 1.9 Ohm.

<img src="img/steps.svg" width="50%">

Below are results from driving a small stepper with 6.7 Ohm resistance.  Vref is set at 2.0V.  Red moving one direction, blue returning.  No load.  Looks good.  But when vref is set to 1.0, the behavior is as before -  uneven stepping, with a big pause at 5-10 steps.
  

<img src="img/step_mar09.svg" width="50%">
T
