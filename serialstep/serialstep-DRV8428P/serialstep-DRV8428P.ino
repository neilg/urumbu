//
// serialstep-DRV8428P.ino
//
// serial step-and-direction, dual-H bridge
//
// Quentin Bolsee 1/31/22
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

// pins
#define LEDA 4
#define LEDC 2
#define NSLEEP 5
#define AIN1 8
#define AIN2 9
#define BIN1 14
#define BIN2 15
#define BUTTON 31

// TCC0 channels
#define AIN1_CH 2
#define AIN2_CH 3
#define BIN1_CH 0
#define BIN2_CH 1

// peripherals
#define AIN1_PER PER_TIMER
#define AIN2_PER PER_TIMER
#define BIN1_PER PER_TIMER_ALT
#define BIN2_PER PER_TIMER_ALT

// any power of 2 <= TABLE_MICROSTEP
#define MICROSTEP 32

// constants
#define TABLE_MICROSTEP 32
#define CYCLE 4
#define TCC0_PERIOD 2400
#define TABLE_LENGTH (CYCLE*TABLE_MICROSTEP)
#define TABLE_INCR (TABLE_MICROSTEP/MICROSTEP)

// macros
#define SYNC_TCC0() while (TCC0->SYNCBUSY.reg & TCC_SYNCBUSY_MASK)
#define INCR_MOD(a, b, c) (((a+b)%c + c) % c)

uint16_t sine_table[] = {
   0, 118, 235, 352, 468, 583, 697, 809, 918,1026,1131,1234,1333,1430,1523,1612,1697,1778,1855,1928,1996,2059,2117,2170,2217,2260,2297,2328,2354,2374,2388,2397,
2400,2397,2388,2374,2354,2328,2297,2260,2217,2170,2117,2059,1996,1928,1855,1778,1697,1612,1523,1430,1333,1234,1131,1026, 918, 809, 697, 583, 468, 352, 235, 118,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0
};

// quarter wave offsets
int count_A1 = 0;
int count_A2 = 2*TABLE_MICROSTEP;
int count_B1 = TABLE_MICROSTEP;
int count_B2 = 3*TABLE_MICROSTEP;

void setup_peripheral(uint8_t pinNum, EPioPeripheral peripheral) {
    uint8_t pinCfg = (PORT->Group[PORTA].PINCFG[pinNum].reg & PORT_PINCFG_PULLEN);
    if ( pinNum & 1 ) { // odd pin
        uint32_t temp = (PORT->Group[PORTA].PMUX[pinNum >> 1].reg) & PORT_PMUX_PMUXE( 0xF ) ;
        PORT->Group[PORTA].PMUX[pinNum >> 1].reg = temp|PORT_PMUX_PMUXO( peripheral ) ;
    } else { // even pin
        uint32_t temp = (PORT->Group[PORTA].PMUX[pinNum >> 1].reg) & PORT_PMUX_PMUXO( 0xF ) ;
        PORT->Group[PORTA].PMUX[pinNum >> 1].reg = temp|PORT_PMUX_PMUXE( peripheral ) ;
    }
    pinCfg |= PORT_PINCFG_PMUXEN; // Enable peripheral mux
    PORT->Group[PORTA].PINCFG[pinNum].reg = (uint8_t)pinCfg;
}

void setup_timer() {
   setup_peripheral(BIN1, BIN1_PER);
   setup_peripheral(BIN2, BIN2_PER);
   setup_peripheral(AIN1, AIN1_PER);
   setup_peripheral(AIN2, AIN2_PER);

   GCLK->CLKCTRL.reg = (uint16_t) (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCM_TCC0) ;
   while ( GCLK->STATUS.bit.SYNCBUSY == 1 );

   TCC0->CTRLA.bit.ENABLE = 0;
   SYNC_TCC0();
   TCC0->WAVE.reg |= TCC_WAVE_WAVEGEN_NPWM;
   SYNC_TCC0();

   TCC0->PER.reg = TCC0_PERIOD;
   SYNC_TCC0();
   TCC0->CTRLA.bit.ENABLE = 1;
   SYNC_TCC0();
   }

void update_pwm(int timerChannel, uint16_t value) {
   TCC0->CTRLBSET.bit.LUPD = 1;
   SYNC_TCC0();
   TCC0->CCB[timerChannel].reg = (uint32_t) value;
   SYNC_TCC0();
   TCC0->CTRLBCLR.bit.LUPD = 1;
   SYNC_TCC0();
   }

void setup() {
   SerialUSB.begin(0);
   digitalWrite(LEDA,HIGH);
   pinMode(LEDA,OUTPUT);
   digitalWrite(LEDC,LOW);
   pinMode(LEDC,OUTPUT);
   digitalWrite(NSLEEP,HIGH);
   pinMode(NSLEEP,OUTPUT);
   pinMode(BUTTON, INPUT_PULLUP);

   // init TCC0 and enable outputs
   setup_timer();
   // init stepping
   step(0);
   }


void step(int val) {
   int val_incr = val*TABLE_INCR;
   count_A1 = INCR_MOD(count_A1, val_incr, TABLE_LENGTH);
   count_A2 = INCR_MOD(count_A2, val_incr, TABLE_LENGTH);
   count_B1 = INCR_MOD(count_B1, val_incr, TABLE_LENGTH);
   count_B2 = INCR_MOD(count_B2, val_incr, TABLE_LENGTH);

   update_pwm(AIN1_CH, sine_table[count_A1]);
   update_pwm(AIN2_CH, sine_table[count_A2]);
   update_pwm(BIN1_CH, sine_table[count_B1]);
   update_pwm(BIN2_CH, sine_table[count_B2]);
   }


void loop() {
   int i;

   for (i = 0; i < 6400; i++) {
     step(1);
     delayMicroseconds(500);
   }

   for (i = 0; i < 6400; i++) {
     step(-1);
     delayMicroseconds(500);
   }
   return;

   if (SerialUSB.available()) {
      char c = SerialUSB.read();
      if (c == 'f') {
         step(1);
         }
      else if (c == 'r') {
         step(-1);
         }
      else if (c == '?') {
         // reply with button value
         int btn = digitalRead(BUTTON);
         SerialUSB.write(btn ? '1' : '0');
         }
      }
   }
