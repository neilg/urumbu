## Short (50mm)_z axis.

Below is a short z-axis integrated into the xy machine.  The z axis has about 50mm travel.  Next, demonstrate wax cutting and PCB milling with this configuration, as well as measuring z-axis resolution and hysteresis.

<img src=./img/z-axis.jpg width=40%>

<img src=./img/xyz_machine.jpg width=40%><br><br>


String up and measure z-axis alone.  First horizontal, with fishing line as belt.  Necessary to adjust bearing position to ease friction on z-travel.  Once that feels good, the plot below is obtained, moving in increments of 32*(1/32) steps eight times forward, then reversing.  Each full step is calculated, using the radius of the capstan, to be 0.31 mm.  Hysteresis is about 0.05 mm.

<img src=./img/z_mar08.svg width=75%><br>

Now do the same with axis held vertically.  Result below.  Looks like hysteresis is now about 0.13 mm.

<img src=./img/z_mar11.svg width=75%><br
