<pre>

materials

Aluminum
   69 GPa tensile modulus, 110 MPa tensile strength
   Misumi CAF5-2020-100
   10x2x2 cm
   $3.92 untapped, $7.87 tapped
   0.0014, 0.0028 $/GPa/cm^3
Garolite
   8 GPa tensile modulus, 50 MPa tensile strength
   McMaster 8491K26
   24x36x1/4" $107
   40 cm^3 = $1.20
   0.0037 $/GPa/cm^3
PLA
   4 GPa tensile modulus, 60 MPa tensile strength
   $20/kg
   1.24 g/cm^3
   40 cm^3 = $0.99
   0.0061 $/GPa/cm^3
HDPE
   1 GPa tensile modulus, 20 MPa tensile strength
   McMaster 8619K466
   24x36x1/4" $39.01
   40 cm^3 = $0.44
   0.011 $/GPa/cm^3

motor control

296-DRV8428PWPRCT-ND
DRV8428PWPR
Texas Instruments
35V, 1A BIPOLAR STEPPER MOTOR DR
$2.57300
New: https://www.ti.com/product/DRV8428
dual H-bridge
TSSOP16
step+direction, microstepping
4.2-33V
1.5 Ohm RDS
1A/bridge, 1.7A peak
internal PWM
integrated current sense
1/8-1/256 microstepping
smart tune decay

296-53424-1-ND
DRV8847PWPR
Texas Instruments
2A DUAL H-BRIDGE MOTOR DRIVER
$1.826
dual H-bridge
HTSSOP16
2.7-18V
1 Ohm RDS
1A/bridge
forward/reverse/brake
independent half-bridge

trim pot

ST4ETB103CT-ND	
ST4ETB103
Nidec Copal Electronics
TRIMMER 10K OHM 0.25W GW TOP ADJ

power capacitor

10-EEE-FN1E101ULCT-ND
EEE-FN1E101UL
Panasonic Electronic Components
CAP ALUM 100UF 25V 20% SMD
0.35700

motors

https://www.jameco.com/z/STP-42D201-37-Shinano-Kenshi-12-Volt-1-8-Step-Angle-Bipolar-Stepper-Motor_2158531.html
12 Volt 1.8 Step Angle Bipolar Stepper Motor
Jameco Part no.: 2158531
$4.95

https://www.jameco.com/z/1124176-Jameco-Reliapro-Bipolar-Stepper-Motor-1-8-VDC-1500-mA-1-8-deg-200-steps_2245626.html
Bipolar Stepper Motor 1.8 VDC 1500 mA 1.8°/200 steps
Jameco Part no.: 2245626
$6.95

https://www.jameco.com/z/1124142-Jameco-Reliapro-Bipolar-Stepper-Motor-4-5-VDC-1500-mA-1-8-deg-200-steps_2245589.html
Bipolar Stepper Motor 4.5 VDC 1500 mA 1.8°/200 steps
Jameco Part no.: 2245589
$9.49

USB

https://www.amazon.com/Sabrent-Charging-Individual-Switches-HB-B7C3/dp/B0797NWDCB
Sabrent 60W 10-Port USB 3.0 Hub Includes 3 Smart Charging Ports with Individual Power Switches and LEDs + 60W 12V/5A Power Adapter (HB-B7C3)
Price: $39.98

https://www.amazon.com/Extension-NIMASO-Material-Compatible-Playstation/dp/B088GSJLWX
2 Pack 3.3FT + 6.6FT USB 3.0 Extension Cable, NIMASO USB A Male to Female Extension Cord Durable Material Fast Data Transfer Compatible with Printer, USB Keyboard, Flash Drive, Hard Drive, Playstation
Price: $9.99

servo test

https://www.jameco.com/z/RS001B-Dagu-HiTech-Electronic-9g-2-kg-cm-Micro-Servo-Motor_2214601.html
9g 2 kg.cm Micro Servo Motor
Jameco Part no.: 2214601
Manufacturer: Dagu HiTech Electronic
Manufacturer p/n: RS001B
5+	$4.49
Stall Torque (4.8V)	20.83 oz.in (1.5 kg.cm)

https://www.jameco.com/z/SER0006-DFRobot-9g-Micro-Servo_2213350.html
9g Micro Servo
Jameco Part no.: 2213350
Manufacturer: DFRobot
Manufacturer p/n: SER0006
5+	$5.49
Stall Torque (4.8V)	22 oz.in (1.6 kg.cm)

https://www.jameco.com/z/DG-12g-Dagu-HiTech-Electronic-Micro-Servomotor-12g-1-8kg-cm_2266599.html
Micro Servomotor 12g 1.8kg/cm
Jameco Part no.: 2266599
Manufacturer: Dagu HiTech Electronic
Manufacturer p/n: DG-12g
5+	$5.49
Stall Torque (4.8V)	25 oz.in (1.8 kg.cm)

Standard 0-180 Servo 3kg/cm Stall Torque
Jameco Part no.: 2201615
Manufacturer: Jameco Reliapro
Manufacturer p/n: CYS-S3003
5+	$6.95
Stall Torque (4.8V)	34.71 oz.in (2.5 kg.cm)

motor control test

296-53467-1-ND
DRV8847SPWR
Texas Instruments
PWR MGMT MOTOR/FAN DRIVER
$1.919
dual H-bridge
TSSOP16
2.7-18V
1A/bridge
forward/reverse/brake
independent half-bridge

296-53425-1-ND
DRV8847PWR
Texas Instruments
2A DUAL H-BRIDGE MOTOR DRIVER
$1.826
dual H-bridge
TSSOP16
2.7-18V
1A/bridge
forward/reverse/brake
independent half-bridge

296-38514-5-ND
DRV8848PWP
Texas Instruments
IC MTR DRV BIPOLR 4-18V 16HTSSOP
$2.528
dual H-bridge
HTSSOP16
4-18V
2A/bridge
forward/reverse/brake
independent half-bridge

296-47770-1-ND
DRV8848PWPR
Texas Instruments
IC MTR DRV BIPOLR 4-18V 16HTSSOP
$2.107
dual H-bridge
HTSSOP16
4-18V
2A/bridge
forward/reverse/brake
independent half-bridge

264-TC78H660FNGELCT-ND
TC78H660FNG,EL
Toshiba Semiconductor and Storage
2CH BRUSHED MOTOR DRIVER 18V/2A
$1.17300
dual H-bridge
TSSOP16
2-16V
2.0A
.48 Ohm
stop, forward, reverse
PWM constant-current
voltage current setting
resistor chopping frequency

TC78H651FNG(OEL)CT-ND
TC78H651FNG(O,EL)
Toshiba Semiconductor and Storage
IC MOTOR DRIVER 16TSSOP
$1.18800
dual H-bridge
TSSOP16
1.8-6V supply
1.6A
.22 Ohm
stop, forward, reverse

264-TC78H651AFNGELCT-ND
TC78H651AFNG,EL
Toshiba Semiconductor and Storage
BRUSHED DC MOTOR DRIVER, DUAL-CH
$1.18800
dual H-bridge
TSSOP16
1.8-7.5V
2.0A
.22 Ohm
stop, forward, reverse

296-30428-5-ND
DRV8834PWP
Texas Instruments
IC MOTOR DRIVER BIPOLAR 24HTSSOP
$2.77500
dual H-bridge
TSSOP24
1.5A continuous, 2.2A peak
.3 Ohm
2.5-10.8V
2x DC direction, or 1x stepper step+direction

620-1352-1-ND
A4982SLPTR-T
Allegro MicroSystems
IC MTR DRV BIPOLR 3-5.5V 24TSSOP
$3.20400
dual H-bridge
TSSOP24
8-35V
.3 Ohm
2A
microstepping
PWM current

USB test

https://www.amazon.com/Anker-PowerIQ-Charging-MacBook-Surface/dp/B00VDVCQ84
Anker 10 Port 60W Data Hub with 7 USB 3.0 Ports and 3 PowerIQ Charging Ports for MacBook, Mac Pro/Mini, iMac, XPS, Surface Pro, iPhone 7, 6s Plus, iPad Air 2, Galaxy Series, Mobile HDD, and More
Price: $42.99
 
https://www.amazon.com/Splitter-Transfer-Charging-Individual-Switches/dp/B07DW646GY
APANAGE Powered USB 3.0 Hub, 11 Ports USB Hub Splitter (7 High Speed Data Transfer Ports + 4 Smart Charging Ports) with Individual On/Off Switches and 48W Power Adapter for Mac Pro/mini, PC, HDD, Disk
Price: $36.99
Quantity: 1

0.95440

trim pot test

987-1014-1-ND	
23BR10KLFTR
TT Electronics/BI
TRIMMER 10K OHM 0.25W GW TOP ADJ
0.91800

TC33X-1-103ECT-ND	
TC33X-1-103E
Bourns Inc.
TRIMMER 10K OHM 0.1W J LEAD TOP
0.22760

987-1008-1-ND	
23AR10KLFTR
TT Electronics/BI
TRIMMER 10K OHM 0.25W J LEAD TOP
0.91800

1993-1063-1-ND	
N6L50T8S-103A3030R-E
Amphenol Piher Sensing Systems
POT 10K OHM LINEAR
0.74720

ST4ETA103CT-ND	
ST4ETA103
Nidec Copal Electronics
TRIMMER 10K OHM 0.25W J LEAD TOP
0.95440

1993-1054-1-ND	
PS6KV50-103A3030-PM
Amphenol Piher Sensing Systems
POT 10K OHM LINEAR
0.80360

1993-1056-1-ND	
PS6KV55-103A3030
Amphenol Piher Sensing Systems
POT 10K OHM LINEAR
0.80360

987-1008-1-ND	
23AR10KLFTR
TT Electronics/BI
TRIMMER 10K OHM 0.25W J LEAD TOP
0.91800

987-1014-1-ND	
23BR10KLFTR
TT Electronics/BI
TRIMMER 10K OHM 0.25W GW TOP ADJ
0.91800

1993-1147-1-ND	
PS10KV50-103A3030
Amphenol Piher Sensing Systems
POT 10K OHM LINEAR
0.92560

1993-1146-1-ND	
PS10MV50-103A3030-PM
Amphenol Piher Sensing Systems
POT 10K OHM LINEAR
0.92560

3361P-103GLFCT-ND	
3361P-1-103GLF
Bourns Inc.
TRIMMER 10K OHM 0.5W GW TOP ADJ
0.99920
