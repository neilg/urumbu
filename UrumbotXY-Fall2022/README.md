# UrumbotXY Fall 2022

This project is a [corexy](https://corexy.com/theory.html) 2D motion system using serialstep. Modified from original files in UrumbotXY.

<img src=img/corexy_pen.jpg width=100%><br>

## CoreXY

For a simple coreXY motion system, here is the list of parts you need to make:

<img src=img/doc/corexy_kit.png width=100%><br>

It is advised to 3D print `capstan_motor` and `capstan_pulley`, while all other parts can be machined or laser cut. 3D models files are available under [./files/3d](./files/3d), and dxf files under [./files/2d](./files/2d).

For assembly, you will also need:

- 2x NEMA 17 motors
- 9x bearings
- 8x V bearings
- 4x small bearings
- M5 screws (structure and bearings)
- M5 nuts and washers
- M3 screws (string tensioners)
- M3 nuts and washers

Complete Bill of Materials (BOM) [here](https://docs.google.com/spreadsheets/d/12hboCFKMO40W-K4HecSFnBYBTcj8XZnGsIHQOlsHlmI/edit?usp=sharing)

## Assembly of XY axes.

### Assemble y-plates
<img src=img/y_plate_single.jpg width=300>
<img src=img/y_plate_tab.jpg width=300><br>

y-plate from the top:  single bearing side (left) and tab side, with stack of bearings. (right)<br>

<img src=img/adjuster_detail.png width=300>
<img src=img/double_detail.png width=300>

y_plate: single-bearing side with adjuster (left), and two-bearing side (right).<br>


### Assemble xy-plate and table

<img src=img/xy_bottom.jpg width=300><br>
Bottom view.<br>
<img src=img/xy_detail_1.jpg width=300>
<img src=img/xy_detail_2.jpg width=300><br>
XY_plate and table:  two-bearing side (left) and
one-bearing side with adjuster (right).<br>

### Assemble motor on motor mount.
<img src=img/capstan.jpg width=300>

### Assemble pulleys on pulley mount.

### Assemble X1 frame part with feet.  Add Y1 and Y2 frame members.
<img src=img/assemble_frame.jpg width=300>

### Add motor mounts.
<img src=img/3_beams_assembled.jpg width=300>

### Place xy-plate on x-beam, and attach y-plates.
<img src=img/x_beam_assembly.jpg width=300>
<img src=img/x_beam_assembled.jpg width=300>

### Add to frame.
<img src=img/3_beams_assembled.jpg width=300>


### Add final beam frame member.
<img src=img/add_x2.jpg width=300>


### Add pulley plate and adjust feet.
<img src=img/assembled.jpg width=300>


### Add string.
<img src=img/1_string.jpg width=300>
<img src=img/2_strings.jpg width=300>

Note how the string is inserted on the capstans. The string can be attached to temporary anchor points to provide enough tension during this step. Inside `capstan_pulley`, you should insert a small bearing at the top and bottom. Use appropriate washers to have the correct height; there are 4 levels on `capstan_pulley`, and they should be aligned between each levels of `capstan_motor`.

<img src=img/capstan.jpg width=100%><br>

### Add pen actuator.
<img src=img/pen.jpg width=300>


## Pen plotter

Pen:

<img src=img/corexy_pen.jpg width=100%><br>

Drawing:

<img src=video/corexy_spiral.mp4 width=100%><br>




