# USB 3 power, serial servo PWM

## [serialservo.py](serialservo.py), [serialservo.ino](serialservo.ino)

<img src="http://academy.cba.mit.edu/classes/output_devices/servo/hello.servo-D11C.png" width="75%">

<img src="serialservo.png" width="75%">

<img src="serialservo.mp4" width="75%">
