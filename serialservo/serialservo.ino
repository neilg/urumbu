//
// serialservo.ino
//
// serial servo PWM
//
// Neil Gershenfeld 7/28/21
// Quentin Bolsee 12/7/21 : add button
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#define LEDA 4
#define LEDC 2
#define SERVO 5
#define BUTTON 31

void setup() {
   SerialUSB.begin(0);
   digitalWrite(LEDA,HIGH);
   pinMode(LEDA,OUTPUT);
   digitalWrite(LEDC,LOW);
   pinMode(LEDC,OUTPUT);
   digitalWrite(SERVO,LOW);
   pinMode(SERVO,OUTPUT);
   pinMode(BUTTON,INPUT_PULLUP);
   }

void loop() {
   uint16_t duration;
   if (SerialUSB.available()) {
      SerialUSB.readBytes((char *)&duration,2);
      if (duration == 65535L) {
         // special command, reply with button value
         int btn = digitalRead(BUTTON);
         SerialUSB.write(btn ? '1' : '0');
         }
      else {
         digitalWrite(SERVO,HIGH);
         delayMicroseconds(duration);
         digitalWrite(SERVO,LOW);
         }
      }
   }
