#
# serialservo.py
#    serial servo PWM
#
# Neil Gershenfeld  7/28/21
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#
import serial,sys,time,signal
if (len(sys.argv) != 4):
   print("command line: serialservo.py port baud delay/delta")
   sys.exit()
device = sys.argv[1]
baud = int(sys.argv[2])
delay = float(sys.argv[3])
print('open '+device+' at '+str(baud)+' delay '+str(delay))
port = serial.Serial(device,baudrate=baud,timeout=0)
pwmtime = 20000
mindelay = 1000
maxdelay = 2000
count = mindelay
delta = int(delay)
delayus = int(delay*1000000)
t = 0
#
# event loop version
#
t0 = time.clock_gettime_ns(time.CLOCK_REALTIME)/1e3
while (True):
   t = time.clock_gettime_ns(time.CLOCK_REALTIME)/1e3
   if ((t-t0) >= pwmtime):
      port.write(count.to_bytes(2,byteorder='little'))
      t0 = t
      count += delta
      if ((count >= maxdelay) or (count <= mindelay)):
         delta = -delta
#
# event loop timing test
#
count = 1
dt2 = 1000
dt1 = 19000
dt = dt1
t0 = time.clock_gettime_ns(time.CLOCK_REALTIME)/1e3
while (True):
   t = time.clock_gettime_ns(time.CLOCK_REALTIME)/1e3
   if ((t-t0) > dt):
      port.write(count.to_bytes(1,byteorder='little'))
      t0 = t
      if (dt == dt1):
         dt = dt2
         count = 0
      else:
         dt = dt1
         count = 1
#
# loop timing test
#
count = 10
while (True):
   port.write(count.to_bytes(2,byteorder='little'))
#
# alarm version
#
def handler(signum,stack):
   global step,count,t
   t += delayus
   if (t < pwmtime):
      return
   t = 0
   port.write(count.to_bytes(2,byteorder='little'))
   if (step == 1):
      if (count < maxdelay):
         count += delta
      else:
         step = -1
         count -= delta
   else:
      if (count > mindelay):
         count -= delta
      else:
         step = 1
         count += delta
#
signal.signal(signal.SIGALRM,handler)
signal.setitimer(signal.ITIMER_REAL,delay,delay)
#
while (True):
   0  
