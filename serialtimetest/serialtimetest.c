//
// serialtimetest.c
//    serial speed time test
//
// Neil Gershenfeld  4/11/21
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
//
int speed,port;
float delay;
unsigned char msg0 = 0;
unsigned char msg1 = 255;
//unsigned char buf[4] = {0,100,200,255};
unsigned char buf[1] = {200};
//
void timer_handler (int signum) {
   write(port,&buf,4);
   /*
   static int flag = 0;
   if (flag == 0) {
      flag = 1;
      write(port,&msg0,1);
      }
   else {
      flag = 0;
      write(port,&msg1,1);
      }
   */
   }
//
int main(int argc,char *argv[]) {
   if (argc != 4) {
      printf("command line: serialtimetest port speed delay\n");
      return -1;
      }
   //
   // set up port
   //
   delay = atof(argv[3]);
   speed = atoi(argv[2]);
   port = open(argv[1],O_RDWR|O_NOCTTY|O_NDELAY);
   fcntl(port,F_SETFL,0);
   struct termios term;
   tcgetattr(port,&term);
   term.c_cflag &= ~(PARENB|CSTOPB|CSIZE|CRTSCTS);
   term.c_cflag |= (CS8|CREAD|CLOCAL);
   term.c_lflag &= ~(ICANON|ECHO|ECHOE|ECHONL|ISIG);
   term.c_iflag &= ~(IXON|IXOFF|IXANY|IGNBRK|BRKINT
      |PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
   term.c_oflag &= ~(OPOST|ONLCR);
   term.c_cc[VTIME] = 0;
   term.c_cc[VMIN] = 1;
   cfsetispeed(&term,speed);
   cfsetospeed(&term,speed);
   tcsetattr(port,TCSANOW,&term);
   //
   // alarm version
   //
   struct sigaction sa;
   struct itimerval timer;
   memset (&sa, 0, sizeof (sa));
   sa.sa_handler = &timer_handler;
   sigaction (SIGALRM, &sa, NULL);
   timer.it_value.tv_sec = 0;
   timer.it_value.tv_usec = delay*1.0e6;
   timer.it_interval.tv_sec = 0;
   timer.it_interval.tv_usec = delay*1.0e6;
   setitimer (ITIMER_REAL,&timer,NULL);
   while (1);
   /*
   //
   // loop version
   //
   struct timeval time;
   double newtime,oldtime;
   gettimeofday(&time,NULL);
   oldtime = time.tv_sec+time.tv_usec/1.0e6;
   while (1) {
      gettimeofday(&time,NULL);
      newtime = time.tv_sec+time.tv_usec/1.0e6;
      if ((newtime-oldtime) >= delay) {
         oldtime = newtime;
         //write(port,&buf,4);
         write(port,&buf,1);
         }
      }
   */
   }
