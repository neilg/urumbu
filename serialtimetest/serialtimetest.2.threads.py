#
# serialtimetest.2.threads.py
#    serial speed time test, two ports, threaded version
#
# Neil Gershenfeld  4/30/21
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#
import serial,sys,time,signal,threading
if (len(sys.argv) != 5):
   print("command line: serialtest.2.threads.py port1 port2 speed delay")
   sys.exit()
device1 = sys.argv[1]
device2 = sys.argv[2]
baud = int(sys.argv[3])
delay = float(sys.argv[4])
print('open '+device1+' at '+str(baud)+' delay '+str(delay))
port0 = serial.Serial(device1,baudrate=baud,timeout=0)
print('open '+device2+' at '+str(baud)+' delay '+str(delay))
port1 = serial.Serial(device2,baudrate=baud,timeout=0)
msg = b"\x00"
flag = 0
ports = [port0,port1]
#
# thread handler
#
def fn(id):
   global v,ports,msg
   while (1):
      #
      # wait for flag to be raised
      #
      while (1):
         if (flag != 0):
            break
      ports[id].write(msg)
      #
      # wait for flag to be lowered
      #
      while (1):
         if (flag == 0):
            break
      ports[id].write(msg)
#
# alarm handler
#
def handler(signum,stack):
   global flag,v
   if (flag == 0):
      flag = 1
   else:
      flag = 0
#
# start threads
#
t0 = threading.Thread(target=fn,args=(0,))
t0.start()
t1 = threading.Thread(target=fn,args=(1,))
t1.start()
#
# start alarm
#
signal.signal(signal.SIGALRM,handler)
signal.setitimer(signal.ITIMER_REAL,1,delay)
#
# wait for alarms
#
while (1):
   0  
