# timing

SRM-20: 4 mm/s  
Shopbot: 20 mm/s  

1 mil = 0.0254 mm  
0.1 mil = 0.00254 mm  

SRM-20/1 mil steps = 157 steps/s  
SRM-20/0.1 mil steps = 1,574 steps/s  
SRM-20/0.1 mil steps/10x microstep = 15,740 steps/s  

Shopbot/1 mil steps = 787 steps/s  
Shopbot/0.1 mil steps = 7,874 steps/s  
Shopbot/0.1 mil steps/10x microstep = 78,740 steps/s  

# [SAMD11C embedded code](serialtimetest.D11C.ino)

# [Python host code](serialtimetest.py)

Intel i7-8700T, Ubuntu 20.04

0.2 ms alarm = 1/0.0004 s loop = 2.5 kHz loop = 5 kHz byte rate

## writing to port

<img src=SDS00001.png width=75%><br>
<img src=SDS00002.png width=75%><br>

## writing to serial port

<img src=SDS00005.png width=75%><br>
<img src=SDS00006.png width=75%><br>

# [C host code](serialtimetest.c)

0.1 ms alarm = 10 kHz byte rate

## writing to serial port

<img src=C.SDS00003.png width=75%><br>
<img src=C.SDS00002.png width=75%><br>

# [C packet host code](serialtimetest.c)

1 Mbps serial, 10 kHz 32-bit packets

<img src=C.packet.SDS00001.png width=75%><br>
<img src=C.packet.SDS00004.png width=75%><br>

# Raspberry Pi 4

~ same with nice -n -20, 10 kHz

[Preempt-RT real-time kernel?](https://lemariva.com/blog/2019/09/raspberry-pi-4b-preempt-rt-kernel-419y-performance-test)

<img src=Pi4.SDS00005.png width=75%><br>
<img src=Pi4.SDS00004.png width=75%><br>

# [timer loops](serialtimetest.c)

# [Web serial](serialtimetest.html)

[API](https://web.dev/serial/)

# [sequential write to 2 ports](serialtimetest.2.py)

<img src=2.SDS00003.png width=75%><br>
<img src=2.SDS00004.png width=75%><br>

# [threads](serialtimetest.2.threads.py)

[GIL](https://wiki.python.org/moin/GlobalInterpreterLock)

<img src=threads.png width=75%><br>

# [multiprocessing Events](serialtimetest.2.multi.py)

intermittent recursion errors

<img src=multi-event.png width=75%><br>
<img src=multi-event.1.png width=75%><br>

# [multiprocessing Values](serialtimetest.2.multi.py)

more stable

<img src=multi-values.png width=75%><br>
<img src=multi-values.1.png width=75%><br>

# [Multiple ports](serialtimetest.4.multi.py)

[D11C serial bridge](http://academy.cba.mit.edu/classes/embedded_programming/D11C/hello.D11C.serial.5V.2.png)<br>
<img src=4.jpg width=50%><br>
<img src=4.0.png width=100%><br>
<img src=4.1.png width=100%><br>
<img src=4.2.png width=100%><br>
