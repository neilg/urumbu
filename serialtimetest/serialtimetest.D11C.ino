//
// serialtimetest.D11C.ino
//
// D11C serial speed time test
//
// Neil Gershenfeld 4/11/21
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#define pinout (1 << 30) // define output pin

void setup() {
   SerialUSB.begin(0);
   //Serial1.begin(460800);
   Serial1.begin(1000000);
   SYSCTRL->OSC8M.bit.PRESC = 0; // set OSC8M clock prescaler to 1
   REG_PORT_DIR0 |= pinout; // set output pin
   }

void loop() {
   static unsigned char chr;
   if (SerialUSB.available()) {
      //REG_PORT_OUT0 = SerialUSB.read();
      chr = SerialUSB.read();
      Serial1.write(chr);
      }
   }
