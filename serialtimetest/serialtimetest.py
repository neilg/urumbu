#
# serialtimetest.py
#    serial speed time test
#
# Neil Gershenfeld  4/11/21
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#
import serial,sys,time,signal
if (len(sys.argv) != 4):
   print("command line: serialtest.py port speed delay")
   sys.exit()
device = sys.argv[1]
baud = int(sys.argv[2])
delay = float(sys.argv[3])
print('open '+device+' at '+str(baud)+' delay '+str(delay))
port = serial.Serial(device,baudrate=baud,timeout=0)
msg0 = b"\x00"
msg1 = b"\xff"
flag = 0
'''
#
# loop version
#
oldtime = time.time()
while (1):
   newtime = time.time()
   if ((newtime-oldtime) >= delay):
      oldtime = newtime
      if (flag == 0):
         port.write(msg0)
         flag = 1
      else:
         port.write(msg1)
         flag = 0
'''
#
# alarm version
#
def handler(signum,stack):
   global flag
   if (flag == 0):
      port.write(msg0)
      flag = 1
   else:
      port.write(msg1)
      flag = 0
signal.signal(signal.SIGALRM,handler)
signal.setitimer(signal.ITIMER_REAL,1,delay)
while (1):
   0  
