//
// serialtimetest.2.c
//    serial speed time test
//
// Neil Gershenfeld  4/11/21
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
//
int speed,port1,port2;
float delay;
unsigned char msg0 = 0;
unsigned char msg1 = 255;
//unsigned char buf[4] = {0,100,200,255};
unsigned char buf[1] = {0};
//
void timer_handler (int signum) {
   write(port1,&buf,1);
   write(port2,&buf,1);
   /*
   static int flag = 0;
   if (flag == 0) {
      flag = 1;
      write(port,&msg0,1);
      }
   else {
      flag = 0;
      write(port,&msg1,1);
      }
   */
   }
//
int main(int argc,char *argv[]) {
   if (argc != 5) {
      printf("command line: serialtimetest.2 port1 port2 speed delay\n");
      return -1;
      }
   //
   // set up port
   //
   delay = atof(argv[4]);
   speed = atoi(argv[3]);
   port1 = open(argv[1],O_RDWR|O_NOCTTY|O_NDELAY);
   port2 = open(argv[2],O_RDWR|O_NOCTTY|O_NDELAY);
   fcntl(port1,F_SETFL,0);
   fcntl(port2,F_SETFL,0);
   struct termios term1;
   tcgetattr(port1,&term1);
   term1.c_cflag &= ~(PARENB|CSTOPB|CSIZE|CRTSCTS);
   term1.c_cflag |= (CS8|CREAD|CLOCAL);
   term1.c_lflag &= ~(ICANON|ECHO|ECHOE|ECHONL|ISIG);
   term1.c_iflag &= ~(IXON|IXOFF|IXANY|IGNBRK|BRKINT
      |PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
   term1.c_oflag &= ~(OPOST|ONLCR);
   term1.c_cc[VTIME] = 0;
   term1.c_cc[VMIN] = 1;
   cfsetispeed(&term1,speed);
   cfsetospeed(&term1,speed);
   tcsetattr(port1,TCSANOW,&term1);
   struct termios term2;
   tcgetattr(port1,&term2);
   term2.c_cflag &= ~(PARENB|CSTOPB|CSIZE|CRTSCTS);
   term2.c_cflag |= (CS8|CREAD|CLOCAL);
   term2.c_lflag &= ~(ICANON|ECHO|ECHOE|ECHONL|ISIG);
   term2.c_iflag &= ~(IXON|IXOFF|IXANY|IGNBRK|BRKINT
      |PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
   term2.c_oflag &= ~(OPOST|ONLCR);
   term2.c_cc[VTIME] = 0;
   term2.c_cc[VMIN] = 1;
   cfsetispeed(&term2,speed);
   cfsetospeed(&term2,speed);
   tcsetattr(port2,TCSANOW,&term2);
   //
   // alarm version
   //
   struct sigaction sa;
   struct itimerval timer;
   memset (&sa, 0, sizeof (sa));
   sa.sa_handler = &timer_handler;
   sigaction (SIGALRM, &sa, NULL);
   timer.it_value.tv_sec = 0;
   timer.it_value.tv_usec = delay*1.0e6;
   timer.it_interval.tv_sec = 0;
   timer.it_interval.tv_usec = delay*1.0e6;
   setitimer (ITIMER_REAL,&timer,NULL);
   while (1);
   /*
   //
   // loop version
   //
   struct timeval time;
   double newtime,oldtime;
   gettimeofday(&time,NULL);
   oldtime = time.tv_sec+time.tv_usec/1.0e6;
   while (1) {
      gettimeofday(&time,NULL);
      newtime = time.tv_sec+time.tv_usec/1.0e6;
      if ((newtime-oldtime) >= delay) {
         oldtime = newtime;
         //write(port,&buf,4);
         write(port,&buf,1);
         }
      }
   */
   }
