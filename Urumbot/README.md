The Urumbot (or Ubot) is a 2D drawing machine demonstration of [Urumbu](https://gitlab.cba.mit.edu/neilg/urumbu) motion control, using [nodes powered and addressed over USB](http://academy.cba.mit.edu/classes/output_devices/DRV8428/hello.DRV8428-D11C.png), receiving [serial step-and-direction](https://gitlab.cba.mit.edu/neilg/urumbu/-/blob/master/serialstep/serialstep.ino).

UI:<br>
<img src=UI.png width=50%><br>

Haystack (Alfonso Parra Rubio) version:<br>

<img src=Haystack.jpg width=100%><br>
<img src=Haystack.mp4 width=50%><br>

Simplified (bearing-less) version:<br>

<img src=BearingLessNEMA17.jpg width=75%>
<img src=ForestBeing.jpg width=100%><br>
<img src=ForestBeing.mp4 width=50%><br>

[Servo version](Urumbot.py):

<img src=Servo.mp4 width=100%><br>
