#
# Urumbot-multi.py
#    Urumbu drawing bot demonstration
#    multiprocessing version
#
# Neil Gershenfeld 7/15/21
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#

from tkinter import *
from math import *
import tkinter.filedialog
import serial,signal,multiprocessing,os,random,time
#
# global variables
#
forward = b'f'
reverse = b'r'
workers = []
points = []
plotsize = 250
plotscale = 0
xmin = ymin = 0
#
# shared memory variables
#
SetAlarm = multiprocessing.Value('f',0,lock=False)
Count0 = multiprocessing.Value('i',0,lock=False)
Count1 = multiprocessing.Value('i',0,lock=False)
Step0 = multiprocessing.Value('i',0,lock=False)
Step1 = multiprocessing.Value('i',0,lock=False)
ProbStep0 = multiprocessing.Value('f',0,lock=False)
ProbStep1 = multiprocessing.Value('f',0,lock=False)
#
# open ports and start workers
#
def Connect():
   try:
      Port0 = serial.Serial(Port0Var.get(),baudrate=int(BaudRateVar.get()),timeout=0)
   except:
      print('can not open '+Port0Var.get())
      Port0 = 0
   #
   try:
      Port1 = serial.Serial(Port1Var.get(),baudrate=int(BaudRateVar.get()),timeout=0)
   except:
      print('can not open '+Port1Var.get())
      Port1 = 0
   #
   Motor0Process = multiprocessing.Process(target=MotorWorker0,args=(Port0,))
   Motor0Process.start()
   workers.append(Motor0Process)
   #
   Motor1Process = multiprocessing.Process(target=MotorWorker1,args=(Port1,))
   Motor1Process.start()
   workers.append(Motor1Process)
   #
   AlarmProcess = multiprocessing.Process(target=AlarmWorker)
   AlarmProcess.start()
   workers.append(AlarmProcess)
   #
   ConnectButton.config(background='green')
#
# workers
#
def StartAlarm():
   SetAlarm.value = float(StepTimeVar.get())
#
def MoveCount():
   r = sqrt(Count0.value*Count0.value+Count1.value*Count1.value)
   if (r == 0):
      return
   ProbStep0.value = fabs(Count0.value)/r
   ProbStep1.value = fabs(Count1.value)/r
   R0Var.set(f"{float(R0Var.get())+Count0.value*float(StepSizeVar.get()):.3f}")
   R1Var.set(f"{float(R1Var.get())+Count1.value*float(StepSizeVar.get()):.3f}")
   root.update()
   while 1:
      if ((Count0.value == 0) and (Count1.value == 0)):
         break
   ProbStep0.value = ProbStep1.value = 0
#
def StopAlarm():
   SetAlarm.value = -1
#
def AlarmWorker():
   while (1):
      if (SetAlarm.value > 0):
         signal.signal(signal.SIGALRM,AlarmHandler)
         signal.setitimer(signal.ITIMER_REAL,SetAlarm.value,SetAlarm.value)
         SetAlarm.value = 0
      elif (SetAlarm.value < 0):
         signal.setitimer(signal.ITIMER_REAL,0,0)
         SetAlarm.value = 0
#
def AlarmHandler(signum,stack):
   if (ProbStep0.value != 0):
      Step0.value = 1
   if (ProbStep1.value != 0):
      Step1.value = 1
#
def MotorWorker0(port):
   if (port == 0):
      return
   while (1):
      if (Step0.value == 1):
         if (random.random() <= ProbStep0.value):
            if (Count0.value > 0):
               port.write(forward)
               Count0.value -= 1
            elif (Count0.value < 0):
               port.write(reverse)
               Count0.value += 1
         Step0.value = 0
#
def MotorWorker1(port):
   if (port == 0):
      return
   while (1):
      if (Step1.value == 1):
         if (random.random() <= ProbStep1.value):
            if (Count1.value > 0):
               port.write(forward)
               Count1.value -= 1
            elif (Count1.value < 0):
               port.write(reverse)
               Count1.value += 1
         Step1.value = 0
#
# UI routines
#
def Quit():
   root.destroy()
   for w in workers:
      w.terminate()
   sys.exit()
#
def Jog0CW():
   StartAlarm()
   Count0.value = int(JogStepsVar.get())
   Count1.value = 0
   MoveCount()
   StopAlarm()
#
def Jog0CCW():
   StartAlarm()
   Count0.value = -int(JogStepsVar.get())
   Count1.value = 0
   MoveCount()
   StopAlarm()
#
def Jog1CW():
   StartAlarm()
   Count0.value = 0
   Count1.value = int(JogStepsVar.get())
   MoveCount()
   StopAlarm()
#
def Jog1CCW():
   StartAlarm()
   Count0.value = 0
   Count1.value = -int(JogStepsVar.get())
   MoveCount()
   StopAlarm()
#
def PolarToCartesian(r0,r1):
   x0 = float(X0Var.get())
   y0 = float(Y0Var.get())
   x1 = float(X1Var.get())
   y1 = float(Y1Var.get())
   d = sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0))
   x = (d*d+r0*r0-r1*r1)/(2*d)
   y = sqrt(r0*r0-x*x)
   return (x,y)
#
def CartesianToPolar(x,y):
   x0 = float(X0Var.get())
   y0 = float(Y0Var.get())
   x1 = float(X1Var.get())
   y1 = float(Y1Var.get())
   r0 = sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0))
   r1 = sqrt((x-x1)*(x-x1)+(y-y1)*(y-y1))
   return (r0,r1)
#
def JogAngle():
   StartAlarm()
   r0 = float(R0Var.get())
   r1 = float(R1Var.get())
   x,y = PolarToCartesian(r0,r1)
   theta = float(JogAngleVar.get())*pi/180
   r = int(JogStepsVar.get())*float(StepSizeVar.get())
   xnew = x+r*cos(theta)
   ynew = y+r*sin(theta)
   r0new,r1new = CartesianToPolar(xnew,ynew)
   Count0.value = int((r0new-r0)/float(StepSizeVar.get()))
   Count1.value = int((r1new-r1)/float(StepSizeVar.get()))
   MoveCount()
   StopAlarm()
#
def LoadFile():
   global points,xmin,ymin,plotscale
   filename = tkinter.filedialog.askopenfile()
   file = open(filename.name,'r')
   xmin = ymin = 1e10
   xmax = ymax = -1e10
   digits = ['0','1','2','3','4','5','6','7','8','9']
   arg = ''
   num = ''
   cmd = ''
   points = []
   Plot.create_rectangle(0,0,plotsize,plotsize,outline='',fill='white')
   while 1:
      chr = file.read(1)
      if not chr:
          break
      if (chr == ';'):
         if (num != ''):
            points[-1].append(float(num)/40.0)
            num = ''
         arg = ''
         cmd = ''
      elif (chr == ','):
         if (num != ''):
            points[-1].append(float(num)/40.0)
            num = ''
      else:
         arg += chr
         if (arg == 'PA'):
            cmd = 'PA'
            arg = ''
         elif (arg == 'PR'):
            cmd = 'PR'
            print('plot relative not yet supported')
            arg = ''
         elif (arg == 'PU'):
            cmd = 'PU'
            arg = ''
            points.append(['PU'])
         elif (arg == 'PD'):
            cmd = 'PD'
            arg = ''
            points.append(['PD'])
         elif ((chr in digits) and ((cmd == 'PU') or (cmd == 'PD'))):
            num += chr
   file.close()
   for segment in range(len(points)):
      for point in range(1,len(points[segment]),2):
         x = points[segment][point]
         if (x > xmax): xmax = x
         if (x < xmin): xmin = x
         y = points[segment][point+1]
         if (y > ymax): ymax = y
         if (y < ymin): ymin = y
   FileLimits.config(text=f"{xmax-xmin} x {ymax-ymin}")
   if ((xmax-xmin) > (ymax-ymin)):
      plotscale = (plotsize-2)/(xmax-xmin)
   else:
      plotscale = (plotsize-2)/(ymax-ymin)
   xold = 0
   yold = plotsize-1
   for segment in range(len(points)):
      type = points[segment][0]
      for point in range(1,len(points[segment]),2):
         xnew = 1+plotscale*(points[segment][point]-xmin)
         ynew = (plotsize-1)-plotscale*(points[segment][point+1]-ymin)
         if (type == 'PU'):
            Plot.create_line(xold,yold,xnew,ynew,width=1,fill="#FF0000")
         else:
            Plot.create_line(xold,yold,xnew,ynew,width=1,fill="#000000")
         xold = xnew
         yold = ynew
#
def DrawFile():
   global points,xmin,ymin
   r0 = float(R0Var.get())
   r1 = float(R1Var.get())
   x0,y0 = PolarToCartesian(r0,r1)
   xold = 0
   yold = plotsize-1
   Plot.create_rectangle(0,0,plotsize,plotsize,outline='',fill='white')
   StartAlarm()
   for segment in range(len(points)):
      type = points[segment][0]
      for point in range(1,len(points[segment]),2):
         xnew = 1+plotscale*(points[segment][point]-xmin)
         ynew = (plotsize-1)-plotscale*(points[segment][point+1]-ymin)
         if (type == 'PU'):
            Plot.create_line(xold,yold,xnew,ynew,width=1,fill="#FF0000")
         else:
            Plot.create_line(xold,yold,xnew,ynew,width=1,fill="#000000")
         xold = xnew
         yold = ynew
         x = x0-points[segment][point]+xmin
         y = y0-points[segment][point+1]+ymin
         MoveTo(x,y)
   StopAlarm()
#
def MoveTo(x,y):
   r0new,r1new = CartesianToPolar(x,y)
   r0 = float(R0Var.get())
   r1 = float(R1Var.get())
   Count0.value = int((r0new-r0)/float(StepSizeVar.get()))
   Count1.value = int((r1new-r1)/float(StepSizeVar.get()))
   MoveCount()
#
# set up GUI
#
root = Tk()
root.title('Urumbot-multi.py')
root.configure(bg='white')
pad = 2
#
row = 0
Button(root,text='Quit',command=Quit,bg='white').grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='Step time (s): ',bg='white').grid(column=0,row=row,padx=pad,pady=pad)
StepTimeVar = StringVar(root,'0.0005')
Entry(root,textvariable=StepTimeVar,width=15).grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='Step size (mm):',bg='white').grid(column=0,row=row,padx=pad)
StepSizeVar = StringVar(root,'0.02')
Entry(root,textvariable=StepSizeVar,width=15).grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='Jog steps:',bg='white').grid(column=0,row=row,padx=pad,pady=pad)
JogStepsVar = StringVar(root,'5000')
Entry(root,textvariable=JogStepsVar,width=15).grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='Baud Rate: ',bg='white').grid(column=0,row=row,padx=pad,pady=pad)
BaudRateVar = StringVar(root,'921600')
Entry(root,textvariable=BaudRateVar,width=15).grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
ConnectButton = Button(root,text='Connect',command=Connect,bg='white')
ConnectButton.grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='Motor',bg='white').grid(column=0,row=row,padx=pad,pady=pad)
Label(root,text='Port',bg='white').grid(column=1,row=row,padx=pad,pady=pad)
Label(root,text='Jog',bg='white').grid(column=2,row=row,columnspan=2,padx=pad,pady=pad)
Label(root,text='x (mm)',bg='white').grid(column=4,row=row,padx=pad,pady=pad)
Label(root,text='y (mm)',bg='white').grid(column=5,row=row,padx=pad,pady=pad)
row += 1
Label(root,text='0',bg='white').grid(column=0,row=row,padx=pad,pady=pad)
Port0Var = StringVar(root,'/dev/ttyACM0')
Entry(root,textvariable=Port0Var,width=15).grid(column=1,row=row,padx=pad,pady=pad)
Button(root,text='CW',command=Jog0CW,bg='white').grid(column=2,row=row,padx=pad,pady=pad)
Button(root,text='CCW',command=Jog0CCW,bg='white').grid(column=3,row=row,padx=pad,pady=pad)
X0Var = StringVar(root,'0')
Entry(root,textvariable=X0Var,width=10).grid(column=4,row=row,padx=pad,pady=pad)
Y0Var = StringVar(root,'0')
Entry(root,textvariable=Y0Var,width=10).grid(column=5,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='1',bg='white').grid(column=0,row=row,padx=pad,pady=pad)
Port1Var = StringVar(root,'/dev/ttyACM1')
Entry(root,textvariable=Port1Var,width=15).grid(column=1,row=row,padx=pad,pady=pad)
Button(root,text='CW',command=Jog1CW,bg='white').grid(column=2,row=row,padx=pad,pady=pad)
Button(root,text='CCW',command=Jog1CCW,bg='white').grid(column=3,row=row,padx=pad,pady=pad)
X1Var = StringVar(root,'500')
Entry(root,textvariable=X1Var,width=10).grid(column=4,row=row,padx=pad,pady=pad)
Y1Var = StringVar(root,'0')
Entry(root,textvariable=Y1Var,width=10).grid(column=5,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='Angle (degrees)',bg='white').grid(column=0,row=row,padx=pad,pady=pad)
JogAngleVar = StringVar(root,'0')
Entry(root,textvariable=JogAngleVar,width=15).grid(column=1,row=row,padx=pad,pady=pad)
Button(root,text='Jog Angle',command=JogAngle,bg='white').grid(column=2,columnspan=2,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='R0 (mm):',bg='white').grid(column=0,row=row,padx=pad)
R0Var = StringVar(root,'500')
Entry(root,textvariable=R0Var,width=15).grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='R1 (mm):',bg='white').grid(column=0,row=row,padx=pad)
R1Var = StringVar(root,'500')
Entry(root,textvariable=R1Var,width=15).grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Button(root,text='Load HPGL File',command=LoadFile,bg='white').grid(column=0,row=row,padx=pad,pady=pad)
FileNameVar = StringVar(root,'')
Button(root,text='Draw',command=DrawFile,bg='white').grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='drawing size (mm):',bg='white').grid(column=0,row=row,padx=pad,pady=pad)
FileLimits = Label(root,text='',bg='white')
FileLimits.grid(column=1,row=row,padx=pad,pady=pad)
#
row += 1
Label(root,text='origin',bg='white').grid(column=0,row=row,padx=pad,pady=pad,sticky='se')
Plot = Canvas(root,width=plotsize,height=plotsize,background='white')
Plot.grid(column=1,row=row,columnspan=4,padx=pad,pady=pad,sticky='sw')
#
# start mainloop
#
root.mainloop()
