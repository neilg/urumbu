

def main():
    filename_in = "being.xy"
    filename_out = "being_flipped.xy"

    f_out = open(filename_out, "w")

    x_c = 80
    y_c = 60

    with open(filename_in, "r") as f_in:
        for line in f_in.readlines():
            if line.upper().startswith("UP") or line.upper().startswith("DOWN"):
                f_out.write(line)
            else:
                x, y = [float(x) for x in line.split(",")]
                x = x_c-x
                y = y_c-y
                f_out.write(f"{x}, {y}\n")

    f_out.close()


if __name__ == "__main__":
    main()
