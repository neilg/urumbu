# UrumbotXY

This project is a [corexy](https://corexy.com/theory.html) 2D motion system using serialstep

<img src=img/corexy_pen.jpg width=100%><br>

## CoreXY

For a simple coreXY motion system, here is the list of parts you need to make:

<img src=img/doc/corexy_kit.png width=100%><br>

It is advised to 3D print `capstan_motor` and `capstan_pulley`, while all other parts can be machined or laser cut. 3D models files are available under [./files/3d](./files/3d), and dxf files under [./files/2d](./files/2d).

For assembly, you will also need:

- 2x NEMA 17 motors
- 9x bearings
- 8x V bearings
- 4x small bearings
- M5 screws (structure and bearings)
- M5 nuts and washers
- M3 screws (string tensioners)
- M3 nuts and washers

Here is the main frame after assembly:

<img src=img/corexy.jpg width=100%><br>

Note how the string is inserted on the capstans. The string can be attached to temporary anchor points to provide enough tension during this step. Inside `capstan_pulley`, you should insert a small bearing at the top and bottom. Use appropriate washers to have the correct height; there are 4 levels on `capstan_pulley`, and they should be aligned between each levels of `capstan_motor`.

<img src=img/capstan.jpg width=100%><br>


Here is a video of the motion at constant speed:

<img src=video/corexy_linear.mp4 width=100%><br>


## Pen plotter

Pen:

<img src=img/corexy_pen.jpg width=100%><br>

Drawing:

<img src=video/corexy_spiral.mp4 width=100%><br>

## Circuitboard Milling

Dec. 17-19, 2021.  Set up CoreXY for milling, using a linear axis for z-travel and a spare Roland MDX-20 spindle.  Keep the original python code, removing the homing step and changing speed to 5 mm/s.  

Run a text file for xy motion.  Start, -10.0,-10.0:-10.8,-10.0: -0.8,0.0: 0.0,0.0.

Axis setup:

<img src=img/milling_axes_setup.jpg width=50%><br>

As constructed, end effector responded to sideways force with ~20N/mm.  This led to about 0.5 mm oscillation while milling - not stiff enough.  Added a clamp to stiffen, which allowed milling of traces.  Adjustment of z-axis for milling was accomplished by manual adjustment of bit in collet.  Z-axis motor was energized in order to constrain z-motion. Good enough to demonstrate.

<img src=img/milling_axes_clamp.jpg width=50%><br>

Movie:

<img src=video/milling_movie.mp4 width=100%><br>

Closeup of traces:  Using 1/64" bit and 0.8 mm horizontal separation of paths, at a 45 degree angle, the calculated trace width is 0.3 mm.  Looks pretty close to this.  

<img src=img/traces.jpg width=50%><br>

This machine is close to being able to mill circuitboards.  For the first try, the stiffness of the end effector wrt sideways force seems to be the limiting factor, rather than the xy stiffness.  Next step: a better arrangement of z-stage and full incorporation of the z-motion into the software.

### Small-excursion z-axis design:

A cam translates the spindle by flexing the beam a few mm.  Total excursion at the spindle is about +/- 2 mm.

<img src=img/fixed_z.jpg width=50%><br>
<img src=img/z-cam.jpg width=50%><br>


#### Metrology - components and assembled parts.

Initial results and setups are <a href="https://gitlab.cba.mit.edu/classes/865.21/projects/machinemetrology">here, temporarily.</a>

Fishing line:  3.3 kN

Seven-strand stainless:  11 kN

#### Jan 14, 2022

Using the limited z-excursion setup above, tested g-code parsing python code and ran PCB milling linetest.  Results below are comparable to Roland SRM20 using the same endmill.  Cutting was interrupted by a python write timeout error:  "serial.serialutil.SerialTimooutException: write timeout"  Code used  is urumbu_gcode_1.14.22.py .  Spindle speed in these tests is 16kRPM.

<img src=img/linetest.jpg width=50%><br>

#### Jan 15, 2022

Added an ESC to control the BLDC motor.  We can use the same board used for servo control.  Like the servo, the ESC accepts positive pulses 1-2 ms wide, 20 ms period.  To arm, apply 1ms pulses for one second, and then apply pulse length for desired speed.  With 12V supply, no load speed is 17kRPM at 1.40 ms pulse length.  These pulses need to be applied continuously to run the motor. (this is typical ESC behavior, I think.) Details in Rob's documentation.

<img src=img/bldc_resize.jpg width=50%><br>

## Linear axis

Assembly:

<img src=img/axis.jpg width=100%><br>

Constant speed:

<img src=video/axis_linear.mp4 width=100%><br>

Acceleration:

<img src=video/axis_acceleration.mp4 width=100%><br>
